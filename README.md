# Spring Workshop

Today we will start a multi-day, team exercise in which we explore the Spring framework by building out an application that integrates our **Pricing** and **Investor** compoenents. This application will eventually grow into a Spring Boot application that publishes a REST web service. For today, we're going to focus on assembling a single, coherent Java application, with a **main()** method still but also with meaningful unit and integration tests, in which the **Investor** relies both on the **Pricing** component (no longer needing to be told the price of a stock that's being traded, since it can get the current price here, instead) and the **Market** component (to place trade orders, as before), both via Spring context configuration and dependency injection.

## Criteria for Completed Work

Your project must be available on a public BitBucket repository, the URL of which you'll provide to the instructors for review. Implement as much of the exercise as you can, and whatever you have implemented must compile cleanly. The Maven 'test' goal must run cleanly as well.

You will also demonstrate a working Jenkins project that can run all tests on demand.

At the end of the day, submit your work using this [Google form](https://docs.google.com/forms/d/e/1FAIpQLSf7l_DyhyOzZNIm7E1YqwglNLQECtXQaRF58IECugWuSQi1ig/viewform).

## Setup

Clone the starter project from this URL:

    http://bitbucket.org/wprovost/CitiMiss2019/Workshops/SpringInvestor

This is a complete Maven project, so you can make a copy of it into your workspace, import it, and then link it to your own BitBucket repository. Since this is a team exercise, choose one team member's BitBucket account in which to create the repository, and grant access to the whole team for purposes of Git push, oull requests, etc.

This project merges the reference implementations of the two previous workshops. The two packages, **com.citi.trading** and **com.citi.trading.pricing**, are still independent of each other. There is a new **InvestorApplication**, with a **main()** method that carries out three quick test scenarios, each with its own **Investor** instance on which we make a different stock trade. You can run this class as a Java application, as a baseline, and see the console output from each of the three investors/trades.

Add the **spring-context** and **spring-test** dependencies to your **pom.xml**, and you can remove the **junit**, **hamcrest-all**, and **mockito-core** dependencies now, since they're transitive dependencies of **spring-test**.

Read over the remaining instructions, and decide as a team how you will carry them out: who will do what? Anticipate good times to push, merge, and pull so that you stay reasonably synchronized; try not to leave yourselves witha big ugly merge at the end of the day!

Set up a Jenkins project and link it to your repository. Run the Jenkins build, manually, after any code push, to be certain that your changes are solid.

## Part 1. Convert the **Pricing** class

In this part you'll convert the **Pricing** class to be a fully-featured Spring singleton, and adjust its unit test to take advantage of a Spring test context.

1. Make Pricing a @Component.

1. Define **Connector** as a nested interface that extends **Function<String,InputStream>**. This will make it easier to wire dependencies between this class and various connectors.

1. Make that new inteface the type of the **connector** field, and make that field @Autowired.

1. Rework the default constructor as a **@PostConstruct** method.

1. Remove the other constructor. We're going to trust Spring wiring to support both of the configurations that used to be supported each by one of the two constructors.

1. Make a field **serviceURL**, configurable using a **@Value** annotation. Set that value to the result of a Spring expression that resolves to the value of an application property **com.citi.trading.pricing.Pricing.serviceURL**.

1. Create a properties file **application.properties** with the necessary property defined in it: the value is the current value of the **SERVICE_URL** field in **Pricing**.

1. Now you can get rid of **SERVICE_URL**, and use **serviceURL** instead.

Now refactor **PricingTest** as a Spring test:

1. Annotate the class to **@RunWith** the **SpringRunner** and set your **ContextConfiguration** to **PricingTest.Config.class** (which doesn't exist yet so you'll have a compile error until the next step).

1. Give it a nested class **Config**, annotated as both a **@Configuration** and with **@ComponentScan**.

1. In that class, define a **@Bean** method **connector()** that returns a **Mockito.mock()** of the **Connector** interface.

1. Declare **@Autowired** fields of type **Pricing** (the class under test) and **and Pricing.Connector** (the mock connector, so that you can **prepare()** it distinctly for each test case).

1. Adjust the logic of each test case: don't instantiate anything, just use the auto-wired objects, set up the connector as you want it with **prepare()**, and let the rest of the test case run as before.

1. Run your test and see that it succeeds; fix any issues before proceeding.


## Part 2. Convert the **Market** class

In this part you'll convert the **Market** class to be a fully-featured Spring singleton.

1. Make Market a **@Component**.

1. Note that this class currently hard-codes a handful of strings that are deployment-specific -- not a good practice. For each of these, define a field on the class and initialize it with a **@Value** annotation that evaluates an application property -- as you did for the **serviceURL** on **Pricing**.

1. Add necessary properties to the **application.properties** file.

There is no unit test for **Market**, so you'll wait until the next section to be sure that this conversion has worked.


## Part 3. Convert the **Investor** class

In this part you'll convert the **Pricing** class to be a fully-featured Spring singleton, and adjust its unit test to take advantage of a Spring test context.

1. Make Investor a **@Component**.

1. Make the **market** field **@Autowired**.

1. Remove both constructors.

1. Now one field is auto-wired, but you have no way of setting up the portfolio and cash balance. Add setter methods for each of these.

Now refactor **InvestorTest** as a Spring test:

1. Annotate the class to **@RunWith** the **SpringRunner** and set your **ContextConfiguration** to **PricingTest.Config.class** (which doesn't exist yet so you'll have a compile error until the next step).

1. Give it a nested class **Config**, annotated as both a **@Configuration** -- but no **@ComponentScan** annotation, as you will explicitly publish the beans that you want for this test context.

1. Define two **@Bean** methods: one each to publish a **Market** object and an **Investor** object.

1. Declare **@Autowired** fields of type **Investor**.

1. Adjust the logic of each test case to use this auto-wired instance instead of creating its own.

1. Run the test. You will see that some cases succeed, while others somewhat mysteriously fail. Analyze this: what's the problem, and what's an appropriate fix?

1. Get the test running cleanly before proceeding.

## Part 4. The Payoff

Now you can integrate your components into one application, and in fact simplify how one of them works as it can rely on another one ...

1. Annotate **InvestorApplication** as a **@Configuration** that performs a **@ComponentScan**.

1. At the top of the **main()** method, create an **AnnotationConfigApplicationContext**, with **InvestorApplication** itself as the configuration class.

1. For each of the three test scenarios, instead of creating a new object yourself, call **getBean()** on the application context, asking for a bean of type **Investor.class**. Run the rest of the logic as before.

1. Okay, the big moment: run the application again. All sorts of things could go wrong here, so if it doesn't work right away, don't worry too much. **One** thing that probably won't work yet is, the **Market** class will fail to make connections to ActiveMQ. Let's see what's going on there ...

1. You defined all of the properties that this class requires, in **application.properties**, right? But how does the application know to load this file? Add a **@PropertySource** annotation to call this out explicitly, and remember that it's found not as a file in the working directory, but as a file on the application class path.

1. Still not working? Check that you've defined the necessary properties precisely. This is a weak spot of relying on external properties: if you mistype a property name, it will just quietly fail to resolve and you'll get an empty string.

1. **Still** not working? Try this: **System.out.println()** the value of one of the properties, in the constructor, and then in **placeOrder()**. Run again and see if you're loading the properties at all ...

1. You will most likely find that you are indeed loading them ... but (a) they are valid values in **placeOrder()**, but (b) they are **null** in the constructor! What's going on?

1. The sequence of initialization for a Spring bean is such that the constructor has to be invoked (so that the object exists in memory) __before__ annotations such as **@Value** can be processed. We're doing a lot of initialization in the constructor, and that's too soon to be able to rely on **@Value**-injected values. So: change the constructor to an ordinary method, with no parameters and returning nothing, and annotate is as a **@PostConstruct** method.

But the goal now is to see the three scenarios run, more or less as they did before we got started. Address any issues before proceeding

1. Once it is running without obvious errors, take a look at the console output. Do all of the cash balances make sense?

1. Nope! Something's gone sideways here. It's almost as if, rather than three separate instances of **Investor**, you got just one ... oh, right! This class is now a Spring bean, and by default that means it is a singleton. You can call **getBean()** over and over, and you will always get the same object back. So we're making trades on that one investor -- accidentally resetting its portfolio here, its cash balance there, but all in one, continuous scenario, rather than three separate ones.

1. Add **@Scope("prototype") to the **Investor** class. Now Spring will create new instances for each new caller of **getBean()**.

1. Run again, and now you should have good results all around.

## Stretch Goal

Make the **Investor** component rely on an **@Autowired Pricing** component for current market prices, instead of trusting the caller to tell us the trade price.

1. Define a field of type **Pricing**, and make it **@Autowired**.

1. Remove the **price** parameter from the **buy()** method.

1. Instead, define a **Consumer<PriceData>** using a lambda expression or a method class. Put the current **buy()** implementation in that **apply()** method: create a **Trade** and call **market.placeOrder()**, but now using the closing price of the **PricePoint** that you get pushed to you as a price subscriber.

1. Now, instead of the old implementation of the method, call **subscribe()**, **getPriceData()**, and **unsubscribe()** in quick sequence. The trade order will be placed when you get that call back from the pricing component.

(**InvestorTest** will have errors now. Ignore them for the moment.)

Run **InvestorApplication** and see it run cleanly. Now the results are different, and not as predictable, because we're using dynamic pricing as provided by the backing REST service.

## Super-Stretch Goal

Rework the unit test to fit with the new programming model of the **Investor** class.


