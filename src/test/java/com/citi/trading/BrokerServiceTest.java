package com.citi.trading;

import static org.hamcrest.Matchers.closeTo;
import static org.hamcrest.Matchers.everyItem;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.isA;
import static org.junit.Assert.assertThat;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.trading.InvestorTest.MockMarket;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes=BrokerServiceTest.Config.class)
@DirtiesContext(classMode=DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class BrokerServiceTest {
	
	@Configuration
	@ComponentScan
	public static class Config{
		
		@Bean
		@Scope("prototype")
		public Investor investor() {
			return new Investor();
		}
		
		@Bean
		public OrderPlacer market() {
			return new MockMarket();
		}
	}
	
	
	@Autowired
	private BrokerService db;
	
	
	
	@Test
	public void getAllTest() {
		List<Investor> all = db.getAll();
		assertThat(all, hasSize(3));
		assertThat(all, everyItem(isA(Investor.class)));
	}
	@Test
	public void getByIdTest() {
		Investor investor = db.getByID(1).getBody();
		assertThat(investor.getCash(), closeTo(1000,0.000));
	}
	@Test
	public void openAccountTest() {
		Investor investor = db.openAccount(1234);
		assertThat(investor.getCash(), closeTo(1234,0.000));
	}
	@Test
	public void closeAccountTest() {
		db.closeAccount(1);
		assertThat(db.getAll(), hasSize(2));
	}
}
