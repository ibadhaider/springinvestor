package com.citi.trading;

import static org.hamcrest.Matchers.closeTo;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.hasKey;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.trading.pricing.PriceData;
import com.citi.trading.pricing.PricePoint;
import com.citi.trading.pricing.Pricing;
import com.citi.trading.pricing.Pricing.Connector;

/**
 * Unit test for the {@link Investor} class.
 * We provide a mock {@link Market} to accept outbound orders,
 * and we simulate fills, partial fills, and rejections by calling the 
 * registered <strong>Consumer</strong> directly.
 * 
 * @author Will Provost
 */

@RunWith(SpringRunner.class)
@ContextConfiguration(classes=InvestorTest.Config.class)
public class InvestorTest {

	
	
	
	@Configuration
	public static class Config {
		
		public class MockPricing extends Pricing {
			
			private Map<String,List<Consumer<PriceData>>> subscribers = new HashMap<>();
			private PriceData priceDataMock;
			
			public MockPricing() {
				List<Consumer<PriceData>> msft = new ArrayList<>();
				List<Consumer<PriceData>> khc = new ArrayList<>();
				
				subscribers.put("MSFT", msft);
				subscribers.put("KHC", khc);
				
				final String CSV = "2019-05-31 13:15:30.620,717.6573,740.3884,717.6573,100,35420";
				PricePoint pricePointMock = Pricing.parsePricePoint(CSV);
				
				List<PricePoint> mockData = new ArrayList<>();
				mockData.add(pricePointMock);
				
				priceDataMock = new PriceData("KHC", 1);
				priceDataMock.addData(mockData);

			}
			
			@Override
			public  void subscribe(String symbol, int capacity, Consumer<PriceData> sub) {
				subscribers.get(symbol).add(sub);
			}
			
			@Override
			public  void unsubscribe(String symbol, Consumer<PriceData> sub) {
				subscribers.get(symbol).remove(sub);

			}
			
			@Override
			public  void getPriceData() {
				for (String symbol : subscribers.keySet()) {
					for (Consumer<PriceData> subscriber : subscribers.get(symbol)) {
						subscriber.accept(priceDataMock);
					}
				}

			}
			
		}
		
		@Bean
		public OrderPlacer market() {
			return new MockMarket();
		}
		
		@Bean
		public Investor investor () {
			Map<String,Integer> starter = new HashMap<>();
			Investor inves = new Investor();
			inves.setCash(0);
			inves.setPortfolio(starter);
			return inves;
		}
		
		@Bean
		public Pricing pricing() {
			return new MockPricing();
		}
	}
	
		
	private static Trade pendingTrade;
	private static Consumer<Trade> pendingCallback;
	
	public static class MockMarket implements OrderPlacer {
		public void placeOrder(Trade order, Consumer<Trade> callback) {
			pendingTrade = order;
			pendingCallback = callback;
		}
	}
	
	
	@Autowired
	private Investor investor;
	
	@Before
	public void reset() {
		Map<String,Integer> starter = new HashMap<>();
		investor.setCash(0);
		investor.setPortfolio(starter);
	}
	
	
	private void buyAndConfirm(String stock, int shares, double price) {
		investor.buy(stock, shares);
		pendingTrade.setResult(Trade.Result.FILLED);
		pendingCallback.accept(pendingTrade);
	}
	
	private void buyAndConfirm(String stock, int shares, double price, int partConfirmed) {
		investor.buy(stock, shares);
		pendingTrade.setResult(Trade.Result.PARTIALLY_FILLED);
		pendingTrade.setSize(partConfirmed);
		pendingCallback.accept(pendingTrade);
	}
	
	private void buyAndReject(String stock, int shares, double price) {
		investor.buy(stock, shares);
		pendingTrade.setResult(Trade.Result.REJECTED);
		pendingTrade.setSize(0);
		pendingCallback.accept(pendingTrade);
	}
	
	private void sellAndConfirm(String stock, int shares, double price) {
		investor.sell(stock, shares);
		pendingTrade.setResult(Trade.Result.FILLED);
		pendingCallback.accept(pendingTrade);
	}
	
	private void sellAndConfirm(String stock, int shares, double price, int partConfirmed) {
		investor.sell(stock, shares);
		pendingTrade.setResult(Trade.Result.PARTIALLY_FILLED);
		pendingTrade.setSize(partConfirmed);
		pendingCallback.accept(pendingTrade);
	}
	
	private void sellAndReject(String stock, int shares, double price) {
		investor.sell(stock, shares);
		pendingTrade.setResult(Trade.Result.REJECTED);
		pendingTrade.setSize(0);
		pendingCallback.accept(pendingTrade);
	}
	
	@Test
	public void testBuy() {
		investor.setCash(40000);
		buyAndConfirm("KHC", 100, 100);
		assertThat(investor.getPortfolio(), hasEntry(equalTo("KHC"), equalTo(100)));
		assertThat(investor.getCash(), closeTo(30000.0, 0.0001));
	}
	
	@Test
	public void testSell() {
		Map<String,Integer> starter = new HashMap<>();
		starter.put("MSFT", 10000);
		investor.setPortfolio(starter);
		sellAndConfirm("MSFT", 1000, 100);
		assertThat(investor.getPortfolio(), hasEntry(equalTo("MSFT"), equalTo(9000)));
		assertThat(investor.getCash(), closeTo(100000.0, 0.0001));
	}

	@Test
	public void testSellOut() {
		Map<String,Integer> starter = new HashMap<>();
		starter.put("MSFT", 10000);
		investor.setPortfolio(starter);
		sellAndConfirm("MSFT", 10000, 100);
		assertThat(investor.getPortfolio(), not(hasKey(equalTo("MSFT"))));
	}

	@Test(expected=IllegalStateException.class)
	public void testBuyWithInsufficientCash() {
		buyAndConfirm("KHC", 100, 100);
	}

	@Test(expected=IllegalStateException.class)
	public void testSellWithInsufficientShares() {
		Map<String,Integer> starter = new HashMap<>();
		starter.put("MSFT", 100);
		investor.setPortfolio(starter);
		sellAndConfirm("MSFT", 1000, 100);
	}

	@Test(expected=IllegalStateException.class)
	public void testSellWithNoShares() {
		sellAndConfirm("MSFT", 1000, 100);
	}
	
	@Test
	public void testBuyPartial() {
		investor.setCash(40000);
		buyAndConfirm("KHC", 100, 100, 80);
		assertThat(investor.getPortfolio(), hasEntry(equalTo("KHC"), equalTo(80)));
		assertThat(investor.getCash(), closeTo(32000.0, 0.0001));
	}
	
	@Test
	public void testBuyRejected() {
		investor.setCash(40000);
		buyAndReject("KHC", 100, 100);
		assertThat(investor.getPortfolio(), not(hasKey("KHC")));
		assertThat(investor.getCash(), closeTo(40000.0, 0.0001));
	}
	
	@Test
	public void testSellPartial() {
		Map<String,Integer> starter = new HashMap<>();
		starter.put("MSFT", 10000);
		investor.setPortfolio(starter);
		sellAndConfirm("MSFT", 1000, 100, 500);
		assertThat(investor.getPortfolio(), hasEntry(equalTo("MSFT"), equalTo(9500)));
		assertThat(investor.getCash(), closeTo(50000.0, 0.0001));
	}

	@Test
	public void testSellRejected() {
		Map<String,Integer> starter = new HashMap<>();
		starter.put("MSFT", 10000);
		investor.setPortfolio(starter);

		sellAndReject("MSFT", 1000, 100);
		assertThat(investor.getPortfolio(), hasEntry(equalTo("MSFT"), equalTo(10000)));
		assertThat(investor.getCash(), closeTo(0.0, 0.0001));
	}

	@Test(expected=IllegalArgumentException.class)
	public void testCreateInvalidCash() {
		investor.setCash(-40000);
;
	}

	@Test(expected=IllegalArgumentException.class)
	public void testCreateInvalidShareCount() {
		Map<String,Integer> starter = new HashMap<>();
		starter.put("MSFT", 0);
		investor.setPortfolio(starter);;
	}

	@Test(expected=IllegalArgumentException.class)
	public void testBuyInvalidShareCount() {
		investor.setCash(40000);;
		buyAndConfirm("KHC", 0, 100);
	}
}
