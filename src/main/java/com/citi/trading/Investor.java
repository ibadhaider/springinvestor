package com.citi.trading;

import java.util.Map;
import java.util.function.Consumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.citi.trading.pricing.PriceData;
import com.citi.trading.pricing.Pricing;

/**
 * Encapsulates a stock investor with a portfolio of stocks, some cash,
 * and the ability to place trade orders.
 * 
 * @author Will Provost
 */
@Component
@Scope("prototype")
public class Investor {

	private Map<String,Integer> portfolio;
	
	private int ID;
	
	public int getID() {
		return ID;
	}

	public void setID(int ID) {
		this.ID = ID;
	}

	private double cash;
	
	@Autowired
	private OrderPlacer market;

	@Autowired
	private Pricing pricing;
	
	public void setPortfolio(Map<String, Integer> portfolio) {
		for (int shares : portfolio.values()) {
			if (shares <= 0) {
				throw new IllegalArgumentException("All share counts must be positive.");
			}
		}
		this.portfolio = portfolio;
	}

	public void setCash(double cash) {
		if (cash < 0) {
			throw new IllegalArgumentException("Can't have negative cash.");
		}
		this.cash = cash;
	}
	
	/**
	 * Handler for trade confirmations.
	 */
	private class NotificationHandler implements Consumer<Trade> {
		public void accept(Trade trade) {
			if (trade.getSize() != 0 && trade.getResult() != Trade.Result.REJECTED) {
				synchronized(Investor.this) {
					String stock = trade.getStock();
					if (trade.isBuy()) {
						if (!portfolio.containsKey(stock)) {
							portfolio.put(stock, 0);
						}
						portfolio.put(stock, portfolio.get(stock) + trade.getSize());
						cash -= trade.getPrice() * trade.getSize();
					} else {
						int shares = portfolio.get(stock) - trade.getSize();
						if (shares != 0) {
							portfolio.put(stock, shares);
						} else {
							portfolio.remove(stock);
						}
						cash += trade.getPrice() * trade.getSize();
					}
				}
			}
		}
	}
	private NotificationHandler handler = new NotificationHandler();
	
	/**
	 * Create an investor with some cash but no holdings.
	 */
//	public Investor(double cash, OrderPlacer market) {
//		this(new HashMap<>(), cash, market);
//	}
//	
//	/**
//	 * Create an investor with holdings as a map of tickers and share counts,
//	 * and some cash on hand.
//	 */
//	public Investor(Map<String,Integer> portfolio, double cash, OrderPlacer market) {
//		if (cash < 0) {
//			throw new IllegalArgumentException("Can't have negative cash.");
//		}
//		for (int shares : portfolio.values()) {
//			if (shares <= 0) {
//				throw new IllegalArgumentException("All share counts must be positive.");
//			}
//		}
//		
//		this.portfolio = portfolio;
//		this.cash = cash;
//		this.market = market;
//	}
	
	/**
	 * Accessor for the portfolio.
	 */
	public Map<String,Integer> getPortfolio() {
		return portfolio;
	}
	
	/**
	 * Accessor for current cash. 
	 */
	public double getCash() {
		return cash;
	}
	
	/**
	 * Places an order to buy the given stock.
	 */
	public synchronized void buy(String stock, int size) {
		if (size <= 0) {
			throw new IllegalArgumentException("Must buy a positive number of shares.");
		}
//		if (size * price > cash) {
//			throw new IllegalStateException("Not enough cash to make this purchase.");
//		}
		
		Consumer<PriceData> sub = null;
		try {
			sub = priceData -> {
				double actualPrice = priceData.getData(1).findFirst().get().getClose();
				if (size * actualPrice > cash) {
					throw new IllegalStateException("Not enough cash to make this purchase.");
				}
				Trade trade = new Trade(stock, true, size, actualPrice);
				market.placeOrder(trade, handler);
			};
			
			pricing.subscribe(stock, 1, sub);
			pricing.getPriceData();
		}finally {
			
			pricing.unsubscribe(stock, sub);
		}
		
	}
	
	/**
	 * Places an order to sell the given stock.
	 */
	public synchronized void sell(String stock, int size) {
		if (size <= 0) {
			throw new IllegalArgumentException("Must sell a positive number of shares.");
		}
		if (!portfolio.containsKey(stock) || size > portfolio.get(stock)) {
			throw new IllegalStateException("Not enough shares to make this sale.");
		}
		
		Consumer<PriceData> sub = priceData -> {
			Trade trade = new Trade(stock, false, size, priceData.getData(1).findFirst().get().getClose());
			market.placeOrder(trade, handler);
		};
		
		pricing.subscribe(stock, size, sub);
		pricing.getPriceData();
		pricing.unsubscribe(stock, sub);
	}

	@Override
	public String toString() {
		return String.format("Investor: [cash=%1.4f, portfolio=%s]", 
				cash, portfolio.toString());
	}
}
