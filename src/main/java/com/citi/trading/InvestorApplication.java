package com.citi.trading;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * Test application for the {@link Investor} system.
 * 
 * @author Will Provost
 */
@Configuration
@ComponentScan
@PropertySource("classpath:application.properties")
public class InvestorApplication {

	/**
	 * Create three investors, and for each one request a trade.
	 * Sleep a while to allow for messaging with the stock market,
	 * and then show the updated portfolio and cash balance.
	 */
	
	
	public static void main(String[] args) {
		try (AnnotationConfigApplicationContext context =
				new AnnotationConfigApplicationContext(InvestorApplication.class); ) 
		{
				
				Investor investor1 = context.getBean(Investor.class);
				Investor investor2 = context.getBean(Investor.class);
				Investor investor3 = context.getBean(Investor.class);

				Map<String,Integer> starter1 = new HashMap<>();

				investor1.setCash(40000);
				investor1.setPortfolio(starter1);

				investor1.buy("KHC", 100);
				try {
					Thread.sleep(1500);
				} catch (InterruptedException ex) {
					ex.printStackTrace();
				}
				System.out.println("Investor 1 buys KHC:");
				System.out.println(investor1.getPortfolio());
				System.out.println(investor1.getCash());
				System.out.println();
				
				Map<String,Integer> starter2 = new HashMap<>();
				starter2.put("MSFT", 10000);
				
				investor2.setCash(0);
				investor2.setPortfolio(starter2);

				investor2.sell("MSFT", 1000);
				try {
					Thread.sleep(1500);
				} catch (InterruptedException ex) {
					ex.printStackTrace();
				}
				System.out.println("Investor 2 sells some MSFT:");
				System.out.println(investor2.getPortfolio());
				System.out.println(investor2.getCash());
				System.out.println();
		
				Map<String,Integer> starter3 = new HashMap<>();
				starter3.put("MSFT", 10000);
				investor3.setCash(0);
				investor3.setPortfolio(starter3);
				
				investor3.sell("MSFT", 10000);
				try {
					Thread.sleep(1500);
				} catch (InterruptedException ex) {
					ex.printStackTrace();
				}
				System.out.println("Investor 3 sells all MSFT:");
				System.out.println(investor3.getPortfolio());
				System.out.println(investor3.getCash());
				System.out.println();
			} catch(Exception e) {
				e.printStackTrace();
			}
		
		finally {
				System.exit(0);
			}
		}
	}

