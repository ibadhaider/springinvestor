package com.citi.trading;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("accounts")
public class BrokerService {

	private Map<Integer, Investor> database;// = new HashMap<>();
	private int IDCounter = 1;

	@Autowired
	private ApplicationContext context;
	
	@PostConstruct
	public void init() {
		try {

			this.openAccount(1000);

			this.openAccount(2000);
			Map<String, Integer> starter = new HashMap<>();
			starter.put("MSFT", 100);
			this.getByID(2).getBody().setPortfolio(starter);

			this.openAccount(3000);
			Map<String, Integer> starter3 = new HashMap<>();
			starter.put("KHC", 100);
			this.getByID(3).getBody().setPortfolio(starter3);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public BrokerService() {
		database = new HashMap<Integer, Investor>();
	}
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Investor> getAll(){
		return database.values().stream().collect(Collectors.toList());
	}
	
	@GetMapping(path = "/{ID}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Investor> getByID(@PathVariable int ID) {
		Investor investor = database.get(ID);
		ResponseEntity<Investor> response = new ResponseEntity<Investor>(investor, HttpStatus.OK);
		if (investor == null)  return new ResponseEntity<Investor>(HttpStatus.NOT_FOUND);
		
		return response;
	}

	
	@PostMapping(path = "/openAccount", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.CREATED)
	public Investor openAccount(@RequestParam double cash) {
		Investor investor = context.getBean(Investor.class);
		investor.setID(IDCounter);
		investor.setPortfolio(new HashMap<>());
		investor.setCash(cash);
		
		Map<String, Integer> starter = new HashMap<>();
		investor.setPortfolio(starter);
		
		database.put(IDCounter, investor);
		IDCounter++;
		return investor;
		
	}
	
	@DeleteMapping(path = "/{ID}" )
	public ResponseEntity<Investor> closeAccount(@PathVariable int ID) {
		if (!database.containsKey(ID)) return new ResponseEntity<Investor>(HttpStatus.NOT_FOUND);
		database.remove(ID);
		return  new ResponseEntity<Investor>(HttpStatus.OK);
	}
	
}
